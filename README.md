# Pathfinding Visualizer 
credit: Clement Mihailescu https://www.youtube.com/watch?v=msttfIHHkak


This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

<img src="./public/pathfinding.png" width="700" />

## How to run

This project was built using `create-react-app` so simply clone and run the `npm start` command